start
s
	new serverPort,tcpio,parentPid,childsock,jobCommandErrorFile
	set jobCommandErrorFile="/YDBREDIS/error.txt"
	;
	k ^stef
	;
	set serverPort=3001
	;
	write "Starting Server at port "_serverPort
	;
	; Enable CTRL-C
	use $principal:(ctrap=$zchar(3):exception="use $principal write !,""Caught Ctrl-C, stopping..."",! halt")
	;
	; Device ID
	set tcpio="SCK$"_serverPort
	;
	; Open Code
	s ^stef($zut)="open server"
	open tcpio:(listen=serverPort_":TCP":delim=$zchar(13,10):attach="server"):0:"socket" E  use 0 write "Error: Cannot open port "_serverPort,! quit
	;
	s ^stef($zut)="use server"
	use tcpio:(chset="M")
	;
	s ^stef($zut)="listen"
	write /listen(5) ; Listen 5 deep - sets $KEY to "LISTENING|socket_handle|portnumber"
	set parentPid=$job ; Parent PID for the child process
	;
loop ; wait for connection, spawn process to handle it. GOTO favorite. ;
	;
	; Wait until we have a connection (infinite wait). ;
	for  write /wait(10) quit:$key]""
	;
	;s ^stef($zut)=$key
	; At connection, job off the new child socket to be served away. ;
	if $zpiece($key,"|")="CONNECT" do
	. ;s ^stef($zut)="***CONNECT***"
	. set childsock=$zpiece($key,"|",2)
	. use tcpio:(detach=childsock)
	. new q set q=""""
	. new arg set arg="""SOCKET:"_childsock_""""
	. new $etrap,$estack set $etrap="goto jobError"
	. new j set j="child:(input="_arg_":output="_arg_":error="_q_jobCommandErrorFile_q_":pass:cmd=""child^%ydbwebreq -p "_parentPid_""")"
	. job @j
	goto loop
	;
jobError
	;
	quit
	;
	;
child ; handle HTTP requests on this connection
	new devtmp,i,remoteIp,connectHeader,connectCounter
	new CRLF
	;
	set CRLF=$zchar(13,10)
	set %ydbtcp=$principal ; TCP Device
	set (connectHeader,connectCounter)=0
	;new $etrap set $etrap="goto etsock^%ydbwebreq"
	;s ^stef($zut)="***CHILD"
	;
	;zshow "d":devtmp
	;for i=0:0 set i=$order(devtmp("D",i)) quit:'i  if devtmp("D",i)["REMOTE" set remoteIp=$zpiece($zpiece(devtmp("D",i),"REMOTE=",2),"@")
	;kill devtmp,i
	;s ^stef($zut)=remoteIp	
	use %ydbtcp:(delim=$zchar(13,10):chset="M")
	;
	new command
	set command=0
	new a,b,c
getNext ; 
	read tcpx:1
	if '$zlength(tcpx) goto getNext
	;
	if connectHeader=0 do  goto getNext
	. ;get the header to get the length
	. set connectCounter=connectCounter+1
	. ;set ^stef($zut)="Header found: "_tcpx_" count:"_connectCounter
	. if connectCounter=18 set connectHeader=1 do  goto getNext
	. . use %ydbtcp:(nodelim)
	. . write "-ERR "_$zchar(13,10)
	. . write !
	. . write "-ERR "_$zchar(13,10)
	. . write !
	. . ;write "$3"_CRLF
	. . ;write "+server"_CRLF_"+ydbredis"_CRLF
	. . ;write "+version"_CRLF_"+v0.0.1"_CRLF
	. . ;write "+proto"_CRLF_":2"_CRLF
	. . use %ydbtcp:(delim=$zchar(13,10):chset="M")
	. . ;set ^stef($zut)="connect confirmed"
	;
	;s ^stef($zut)="incoming: "_tcpx
	;
	;s ^stef($zut)="got: "_tcpx
	if command=0,$zextract(tcpx,1,1)="*" set command=$zextract(tcpx,2,9999)*2 goto getNext
	;
	; array 
	if $zextract(tcpx,1,1)="$" set command=command-1 goto getNext
	; simple strings
	set command(command)=tcpx,command=command-1
	; here we have the all array
	if command=0 do  
	. ;merge ^stef($zut)=command
	. ; we have the all (reversed) command, execute 
	. set ^temp(command(5),command(3))=command(1)
	. ; and confirm
	. use %ydbtcp:(nodelim)
	. write ":1"_$zchar(13,10)
	. write !
	. use %ydbtcp:(delim=$zchar(13,10):chset="M")
	. ;s ^stef($zut)="HSET Confirmed"
	. ; reset command array
	. kill command
	. set command=0
	;
	; get more
	goto getNext
	;
	;
	; -- exit on Connection: Close
	;if $zconvert($get(httpreq("header","connection")),"l")="close" 	do  halt
	;. s ^stef($zut)="***CLOSE***"
	;. close %ydbtcp
	;goto getNext
	;
	;
	;
	;
	;
