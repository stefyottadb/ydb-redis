<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# YDBGUI

YottaDB GUI

![main_screen](docs/main-screen.png)

![main_screen](docs/stats.gif)

<hr>

# Getting started

## Pre-requisites

* YottaDB Installation
* Installer utilities: `git`, `cmake`, `pkg-config`, `ld.gold`, `make`
* Operational utilities `df`, `stat`, `ps`, `rm`, `cp`, `grep`,`mv`,'`libsodium-dev` , `gzip` (optional), `date`, `libicu-dev`, `libcurl4-openssl-dev`

## Installation

Installation is done using cmake in the build directory, created in the source directory,
and then running make and make install (using sudo if needed). Here's an example of how
to do it:

```
cd /tmp/
git clone https://gitlab.com/YottaDB/UI/YDBGUI.git
cd YDBGUI
mkdir build && cd build
cmake .. && make && sudo make install
```

If you change versions of YottaDB, you will need to remove the contents of the
build directory and then do the cmake, make, sudo make install step.

The main artifacts that are installed are:

- `_ydbmwebserver.so` in the `plugin/o` directory (and a version for utf8)
- `_ydbgui.so` in the `plugin/o` directory (and a version for utf8)
- The HTML/JS/CSS files (all static) in `plugin/etc`

## Starting the GUI

We recommend sourcing the `ydb_env_set` file (e.g., `. /usr/local/etc/ydb_env_set`
if YottaDB is installed with default options) to set environment variables. If
you use an application where you set-up your own environment variables, you
need to set-up at least these 4 environment variables before starting the GUI:

- `ydb_dist`
- `ydb_gbldir`
- `ydb_routines`
- `ydb_xc_ydbposix`

The GUI relies on the environment variable `ydb_gbldir` to figure out what set
of databases it is going to use. You need to set this up appropriately before
starting the GUI.

`ydb_routines` needs to contain the files
`$ydb_dist/plugin/o/[utf8/]_ydbmwebserver.so`,
`$ydb_dist/plugin/o/[utf8/]_ydbposix.so`
`$ydb_dist/plugin/o/[utf8/]_ydbgui.so`. This can be automatically done by
setting up your `ydb_routines` to contain your directories and then sourcing
`$ydb_dist/ydb_env_set`.

`ydb_xc_ydbposix` needs to be set to `$ydb_dist/plugin/ydbposix.xc`.

Once you set-up these variables, you can start the GUI by doing this (port is
optional, the default is 9080; TLS is also optional):

```
$ydb_dist/yottadb -run %ydbgui [--port nnnn] [--tlsconfig tls-config-name] [--log n] [--gzip] [--readwrite]
```

Once started, visit port 9080 on a web browser on the machine hosting the
YottaDB GUI (http://localhost:9080 if you are doing this on your computer).

A list of the options accepted is as follows:

* `--port nnn` Port Number
* `--tlsconfig tls-config-name` A TLS configuration with a name in the
  `ydb_crypt_config` file. TLS set-up is somewhat complex. See
  https://docs.yottadb.com/ProgrammersGuide/ioproc.html#tls-on-yottadb for
  instructions, and [Dockerfile](Dockerfile) and
  [docker-startup.sh](docker-configuration/docker-startup.sh) for its
  implementation.
* `--log n` A logging level. By default the level is 0. It can be 0-3, with 3
  being most verbose.
* `--gzip` Enable gzipping from the server side.
* `--auth-file` A pointer to the users.json file. See details below.
* `--readwrite` Starts the GUI in `readwrite` mode. See details below.

If `--auth-file` is specified, the GUI requires a login based on the credentials stored in that file. For details about setting up this file, adding / removing users and change passwords, visit the YDB-Web-Server repo: https://gitlab.com/YottaDB/Util/YDB-Web-Server.

In readonly mode (when no `--readwrite` is specified), the following functionality will be disabled:

- Octo: query execution (you can still inspect tables, views and functions)
- Regions:
    - Add
    - Edit
    - Delete
- Region:
    - Create database file
    - Extend database file
    - Switch journal start / stop
    - Switch journal
    - Backup
- Integrity check
- Reorg
- Locks Manager:
    - Clear lock
    - Terminate lock owner

This applies also to the REST server. Trying to execute a RW call in RO mode will throw a `403: Operation not supported in the current server mode`.

## Stopping the GUI

Use `stop^%ydbgui`

```
$ydb_dist/yottadb -run stop^%ydbgui 
```

## Monitoring the database

You can monitor the database and display data samples in graph(s) and / or table(s).

For a description of this functionality, visit this link using:

**http[s]://{host}:{port_number}/help/index.html?stats/index**

## Using the Dockerfile

Instructions for using the Dockerfile are embedded as comments there.

## Executing the Unit Tests

Instructions for executing Unit Tests and re-generate the test list are to be found [here](docs/testing.md)

## REST Api

The web server exposes a collection of REST endpoints for you to use to embed the YDBGUI functionality in your application.

A full description of the API can be found [here](docs/rest.md)

## Setting up the pre-commit hook

If you are developing tests, you should execute the following in order to automatically update `wwwroot/test/README.md`.

```
ln -s ../../pre-commit .git/hooks/pre-commit
```

## Resetting the browser storage

Changes to user settings (e.g. colors for globals) are stored inside the
browser's "local storage". These values override the default settings that come
with the Web Server. If you wish to discard your changes and reset the values
to their default, append `?storage=reset` to the URL.

Example:
`http://localhost:9080/?storage=reset`

## Logging the Web Sockets server operations

In order to switch on disk logging for the Web Socket server, append `?ws-logging` to the URL.

Example:
`http://localhost:9080/?ws-logging`

## Apache Reverse Proxy Configuration

Here are some sample Apache configurations to customize to enable you to
run a reverse proxy to the GUI using Apache.

For the examples below, let's assume that the GUI is on your local network at
`127.0.0.1:9080`.

The simplest configuration to use is a VirtualHost based configuration, which is very simple:

First enable these modules:

```
a2enmod proxy
a2enmod proxy_http
a2enmod proxypass
```

Then set-up this VirtualHost:

```
<VirtualHost *:80>
    ServerName xxx
    ProxyPass / http://127.0.0.1:9080/ nocanon
    ProxyPassReverse / http://127.0.0.1:9080/
</VirtualHost>
```

The second configuration using a URL mapping is more complex. For the purposes
of this example, we will be mapping a URL `/ydbgui/` to `127.0.0.1:9080`.

First, enable the following modules:

```
a2enmod proxy
a2enmod proxy_http
a2enmod proxypass
a2enmod proxy_html
a2enmod xml2enc
a2enmod substitute
a2enmod filter
```

Second, Apache 2.4 does not include proxy-html.conf (but you should
double-check). You need to add this data to your configuration file.

```
ProxyHTMLLinks  a               href
ProxyHTMLLinks  area            href
ProxyHTMLLinks  link            href
ProxyHTMLLinks  img             src longdesc usemap
ProxyHTMLLinks  object          classid codebase data usemap
ProxyHTMLLinks  q               cite
ProxyHTMLLinks  blockquote      cite
ProxyHTMLLinks  ins             cite
ProxyHTMLLinks  del             cite
ProxyHTMLLinks  form            action
ProxyHTMLLinks  input           src usemap
ProxyHTMLLinks  head            profile
ProxyHTMLLinks  base            href
ProxyHTMLLinks  script          src for

ProxyHTMLEvents onclick ondblclick onmousedown onmouseup \
                onmouseover onmousemove onmouseout onkeypress \
                onkeydown onkeyup onfocus onblur onload \
                onunload onsubmit onreset onselect onchange
```

Third, create the following config:

```
ProxyRequests Off
<Location /ydbgui/>
    ProxyPass http://127.0.0.1:9080/
    ProxyPassReverse http://127.0.0.1:9080/
    ProxyHTMLEnable On
    ProxyHTMLURLMap / /ydbgui/
    AddOutputFilterByType SUBSTITUTE text/javascript
    AddOutputFilterByType SUBSTITUTE application/javascript
    AddOutputFilterByType SUBSTITUTE x-application/javascript
    Substitute "s#/api/#/ydbgui/api/#i"
    Substitute "s#/help/#/ydbgui/help/#i"
</Location>
```

## License

This software is released under the terms of the  [GNU Affero General Public License version 3](https://www.gnu.org/licenses/agpl-3.0.txt)
