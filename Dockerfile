#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#	This source code contains the intellectual property	        #
#	of its copyright holder(s), and is made available	        #
#	under a license.  If you do not know the terms of	        #
#	the license, please stop and do not read further.	        #
#                                                               #
#################################################################


FROM yottadb/yottadb-base
# Extra's to run non-interactive Chrome
RUN apt-get update && apt-get install -y curl unzip wget cmake git gcc make \
			npm libssl-dev libconfig-dev libgcrypt-dev libgpgme-dev libicu-dev libsodium-dev \
			curl libcurl4-openssl-dev redis-server

# Initialize files for working directory
RUN mkdir -p /YDBREDIS
WORKDIR /YDBREDIS

ENV NODE_VERSION 18.19.0

RUN ARCH= && dpkgArch="$(dpkg --print-architecture)" \
  && case "${dpkgArch##*-}" in \
    amd64) ARCH='x64';; \
    ppc64el) ARCH='ppc64le';; \
    s390x) ARCH='s390x';; \
    arm64) ARCH='arm64';; \
    armhf) ARCH='armv7l';; \
    i386) ARCH='x86';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  # gpg keys listed at https://github.com/nodejs/node#release-keys
  && set -ex \
  && for key in \
    4ED778F539E3634C779C87C6D7062848A1AB005C \
    141F07595B7B3FFE74309A937405533BE57C7D57 \
    74F12602B6F1C4E913FAA37AD3A89613643B6201 \
    DD792F5973C6DE52C432CBDAC77ABFA00DDBF2B7 \
    61FC681DFB92A079F1685E77973F295594EC4689 \
    8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
    C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
    890C08DB8579162FEE0DF9DB8BEAB4DFCF555EF4 \
    C82FA3AE1CBEDC6BE46B9360C43CEC45C17AB93C \
    108F52B48DB57BB0CC439B2997B01419BD92F80A \
  ; do \
      gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys "$key" || \
      gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key" ; \
  done \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH.tar.xz" \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
  && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
  && grep " node-v$NODE_VERSION-linux-$ARCH.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
  && tar -xJf "node-v$NODE_VERSION-linux-$ARCH.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
  && rm "node-v$NODE_VERSION-linux-$ARCH.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs \
  # smoke tests
  && node --version \
  && npm --version


# Install npm testing packages
COPY package.json /YDBREDIS/package.json
RUN npm install

# Install Encryption Plugin
ENV ydb_dist "/opt/yottadb/current"
ENV ydb_icu_version "70"
RUN git clone https://gitlab.com/YottaDB/Util/YDBEncrypt
RUN cd YDBEncrypt && make install

ENV ydb_xc_libcurl "/opt/yottadb/current/plugin/libcurl.xc"

# Create Certificates
RUN mkdir -p /YDBREDIS/certs
RUN openssl genrsa -aes128 -passout pass:ydbredis -out /YDBREDIS/certs/ydbredis.key 2048
RUN openssl req -new -key /YDBREDIS/certs/ydbredis.key -passin pass:ydbredis -subj '/C=US/ST=Pennsylvania/L=Malvern/CN=localhost' -out /YDBREDIS/certs/ydbredis.csr
RUN openssl req -x509 -days 365 -sha256 -in /YDBREDIS/certs/ydbredis.csr -key /YDBREDIS/certs/ydbredis.key -passin pass:ydbredis -out /YDBREDIS/certs/ydbredis.pem

# Set-up YottaDB Certificate Config
COPY docker-configuration/ydbgui.ydbcrypt /YDBREDIS/certs/
ENV ydb_crypt_config /YDBREDIS/certs/ydbgui.ydbcrypt


ENV gtm_lct_stdnull=1
ENV gtm_lvnullsubs=2

# Tell Node to ignore the self-signed certificate
ENV NODE_TLS_REJECT_UNAUTHORIZED 0

# Install GUI
COPY CMakeLists.txt /build/CMakeLists.txt
COPY _ydbredis.manifest.json /build/_ydbredis.manifest.json
COPY routines /build/routines/
#RUN cd /build/ && mkdir build && cd build && cmake .. && make && make install

COPY docker-configuration/docker-startup.sh /YDBREDIS/docker-startup.sh
COPY docker-configuration/dev /YDBREDIS/dev
COPY js /YDBREDIS/js
# Default environment
RUN echo ". /YDBREDIS/dev" >> $HOME/.bashrc
# Mount point directories. Empty by default.
RUN mkdir /YDBREDIS/routines /YDBREDIS/mwebserver /YDBREDIS/objects /YDBREDIS/wwwroot

ENTRYPOINT ["/YDBREDIS/docker-startup.sh"]

# to build the image
# docker image build --progress=plain -t ydbredis .

# to run the machine
# docker run --rm --name=ydbgui -p 8089:8089 ydbredis

# to enter development mode
# (passing volumes is optional: but it lets you change the code and see the changes immediately applied on the fly)
# in Linux:   docker run -d --init --name=ydbredis -v C:\Users\stefa\WebstormProjects\ydbRedis/js:/YDBREDIS/js -v C:\Users\stefa\WebstormProjects\ydbRedis/routines:/YDBREDIS/routines ydbredis
# to get the user authentication to work, append this: --auth-file /YDBGUI/wwwroot/test/users.json
# to start in utf8, add the following: --env ydb_chset='UTF-8' to the docker run command
# Then, docker exec -it ydbguidev bash

