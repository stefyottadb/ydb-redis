import {createClient} from 'redis';


const client = createClient()
//const client = createClient({url: 'redis://localhost:3001'})
client.on('error', err => console.log('Redis Client Error', err))
await client.connect()

//console.log('Client ready: ', client.isReady)

/*
let max = 100000;
console.log('Redis performance test');
console.log('Insert and read back ' + max.toLocaleString() + ' key/value pairs using HSET and HGET');
console.log('Please wait...')
console.log('-----');

let results = []

 */

try {
    console.log(await client.MSET({
        'test1': '22',
        'test2': '33'
    }))
} catch (e) {
    console.log(e)
}

/*
for (let run = 0; run < 5; run++) {
    console.log('Run # ',run)

    let start = Date.now();

    for (let key = 1; key < max; key++) {
        //console.log('exec HSET: ', key)
        await client.HSET('redistest', key, 'hello world')
        //console.log('done with ', key)
    }

    let finish = Date.now();
    let elap = (finish - start) / 1000;
    console.log('finished ' + max.toLocaleString() + ' inserts in ' + Math.trunc(elap) + ' seconds');
    console.log('rate: ' + Math.trunc((max / elap)).toLocaleString() + ' /sec');
    console.log('------');
    results[run] = Math.trunc((max / elap))
}

let result = 0
results.forEach(run => result +=run)
result = result / results.length

console.log('Mean value:', result)

 */

/*
let start = Date.now();


for (let key = 1; key < max; key++) {
    //console.log('GEtting key: ', key)
    let value = await client.HGET('redistest', key.toString());
    //console.log(value)
}

//finish = Date.now();
//elap = (finish - start)/1000;
console.log('finished ' + max.toLocaleString() + ' gets in ' + Math.trunc(elap) + ' seconds');
console.log('rate: ' + Math.trunc((max / elap)).toLocaleString() + ' /sec');


 */

await client.disconnect();

